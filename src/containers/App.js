import React, { useState } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import './App.css';
import { initialTodos } from '../initialTodos.js'
import { UserInput, TodoList } from '../components';


function App() {

    // Declaring state with Hooks
    const [todos, setTodos] = useState(initialTodos);
    const [newAdded, setNewAdded] = useState(false);

    return (
        <div>
            <Navbar bg="primary" variant="dark">
                <Navbar.Brand>Todo App</Navbar.Brand>
            </Navbar>

            <Container>

                <TodoList
                    todos={todos}
                    newAdded={newAdded}
                    setNewAdded={setNewAdded.bind(this)}
                    setTodos={setTodos.bind(this)}
                />

                <UserInput
                    todos={todos}
                    setTodos={setTodos.bind(this)}
                    setNewAdded={setNewAdded.bind(this)}
                />
            </Container>
        </div>
    );
}

export default App;


