import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt, faCheckSquare, faSquare } from '@fortawesome/free-solid-svg-icons'


export function TodoItem(props) {

    return (
        <Row className="wrapper" style={{ backgroundColor: props.todo.complete ? '#ADD8E6' : '#F0FFFF' }}>
            <Col className="check" onClick={() => props.onClick(props.todo.id)} sm={2} xs={2} md={1}>
                <div className={"action-text-container"}>
                    <FontAwesomeIcon icon={props.todo.complete ? faCheckSquare : faSquare} />
                </div>
            </Col>

            <Col onClick={() => props.onClick(props.todo.id)} sm={8} xs={8} md={10} className="todo-content">
                <h4 className={props.todo.complete ? 'complete' : 'not-complete'}>{props.todo.name}</h4>
            </Col>

            <Col sm={2} xs={2} md={1} className="remove" onClick={() => props.onRemoveClick(props.todo.id)}>
                <div className={"action-text-container"}><FontAwesomeIcon icon={faTrashAlt} /></div>
            </Col>
        </Row>
    );
}