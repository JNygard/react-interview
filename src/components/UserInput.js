import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';


export function UserInput(props) {

    const [newTodoName, setNewTodoName] = useState('');

    var onSubmit = (event) => {
        event.preventDefault();
        props.setNewAdded(true)
        props.setTodos(addTodo(newTodoName, props.todos))
        setNewTodoName('')
    }

    return (
        <Form
            className="user-input"
            onSubmit={onSubmit}>
            <InputGroup size="lg">
                <Form.Control
                    type="text"
                    placeholder="New todo"
                    value={newTodoName}
                    onChange={(e) => setNewTodoName(e.target.value)}
                />
                <Button
                    disabled={newTodoName.length<=0}
                    variant="success"
                    type="submit"
                    value="Submit">
                    Add
                </Button>
            </InputGroup>
        </Form>
    );
}

export function addTodo(newTodo, todoList){
    if (newTodo.length > 0) {
        return  [...todoList, { id: performance.now(), name: newTodo, complete: false }]
    }
    return todoList
}