import { addTodo } from './UserInput'
import { initialTodos } from '../initialTodos.js'

const averageTodo = "Average length todo"
const longEnoughTodo = "A"
const tooShortTodo = ""


// Tests for addTodo function ---------------------

test('Todos incremented by 1 after adding todo', () => {
    const newList = addTodo(longEnoughTodo, initialTodos)
    expect(newList.length).toBe(initialTodos.length + 1);
});

test('Todos NOT incremented after adding too short todo', () => {
    const newList = addTodo(tooShortTodo, initialTodos)
    expect(newList.length).toBe(initialTodos.length);
});

test('Found the new todo from the list', () => {
    const todoToAdd = averageTodo
    let found = false
    const newList = addTodo(todoToAdd, initialTodos)

    newList.forEach(todo => {
        if (todo.name === todoToAdd) {
            found = true
        }
    }
    );
    expect(found).toBeTruthy()
});



