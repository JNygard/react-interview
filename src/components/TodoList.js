import React, { useEffect } from 'react';
import { TodoItem } from './TodoItem';


export function TodoList(props) {

    let todosEnd = React.createRef();

    useEffect(() => {
        if (props.newAdded) {
            // Scroll to bottom after new todo
            todosEnd.scrollIntoView({ behavior: "smooth" })
            props.setNewAdded(false)
        }
    });

    var onCompleteClick = (id) => {
        props.setTodos(toggleComplete(id, props.todos))
    }

    var onRemoveClick = (id) => {
        if (window.confirm("Remove note permanently?")) {
            props.setTodos(removeTodo(id, props.todos))
        }
    }

    return (
        <div className="todo-list">
            {props.todos.map(todo =>
                <TodoItem
                    key={todo.id}
                    todo={todo}
                    onClick={onCompleteClick.bind(this)}
                    onRemoveClick={onRemoveClick.bind(this)}
                />
            )}
            <div style={{ float: "left", clear: "both" }}
                ref={(el) => { todosEnd = el; }}>
            </div>
        </div>
    );
}

export default TodoList;


export function removeTodo(todoID, todoList) {
    return todoList.filter((todo) => { return todo.id !== todoID })
}

export function toggleComplete(todoID, todoList) {
    var newTodos = todoList.slice();
    var i = newTodos.findIndex((obj => obj.id === todoID));
    newTodos[i] = { ...newTodos[i] };
    newTodos[i].complete = !newTodos[i].complete;

    return newTodos
}