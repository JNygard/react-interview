import { removeTodo, toggleComplete } from './TodoList'
import { initialTodos } from '../initialTodos.js'


// Tests for removeTodo function ---------------------


test('Todos decreased after removeTodo', () => {
    const id = initialTodos[0].id
    const newList = removeTodo(id, initialTodos)
    expect(newList.length).toBe(initialTodos.length - 1);
});


test('Todos not decreased after removeTodo with nonexisting ID', () => {
    const id = 99
    const newList = removeTodo(id, initialTodos)
    expect(newList.length).toBe(initialTodos.length);
});


test('Todo deleted not found in the list after removing it', () => {
    const id = initialTodos[0].id
    const newList = removeTodo(id, initialTodos)
    expect(newList.length).toBe(initialTodos.length - 1);
});


// Tests for toggleComplete function ---------------------


test('Spesific todos complete status changes after toggleComplete', () => {
    const testTodo = initialTodos[0]
    const newList = toggleComplete(testTodo.id, initialTodos)
    const todoToggled = newList.find(todo => todo.id === testTodo.id);
    expect(testTodo.complete).toBe(!todoToggled.complete);
});

