

# Todo App task

Todo App task refactored by Joonas Nygård for Buutti. 

- React version upgraded from 16.6.3 -> 16.12.0 in able to use Hooks to declare the state with functional components
- React scripts updated to resolve vulnerabilities
- Third party libraries used to improve the UI
- Jest used for unit tests



## Getting started:

Clone repository, install dependencies and start app
```bash
git clone https://JNygard@bitbucket.org/JNygard/react-interview.git
cd react-interview
npm install
npm run start
```

Run tests
```bash
npm run test
```


## Libraries used

* [React Bootstrap](https://react-bootstrap.github.io/) - UI components
* [React Fontawesome](https://github.com/FortAwesome/react-fontawesome) - Icons

---

This app is intended as a React pre-task for applying to Buutti as a developer.

## Tasks:

1. Refactor this app with the following criterion:

-   Use only functional components DONE
-   Give it a good architectural structure DONE
-   Change the UI look to something better. You choose the style. DONE

2. Write unittests to test 1 component well. DONE

## To return:

-   Fork this repo to your own github, gitlab or bitbucket account and send the interviewer the link to that repo.



